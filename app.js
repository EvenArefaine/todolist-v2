// Import required modules
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const dotenv = require("dotenv")
const _ = require("lodash");
dotenv.config();

// Create an Express application
const app = express();

const username = process.env.u_Name
const password = process.env.p_Word

// Set the view engine to EJS
app.set('view engine', 'ejs');

// Use body-parser to parse incoming request bodies
app.use(bodyParser.urlencoded({ extended: true }));

// Serve static files from the "public" directory
app.use(express.static("public"));

// Connect to the database
mongoose.connect("mongodb+srv://" + username + ":" + password + "@cluster0.qbr6cwh.mongodb.net/todolistDB");

// Define the Mongoose schema for to-do items
const itemsSchema = new mongoose.Schema({
  name: String
});

// Create a Item Mongoose model based on the schema
const Item = mongoose.model("Item", itemsSchema);

// Define the default to-do item
const item1 = new Item({
  name: "Welcome to your todolist!"
});

const item2 = new Item({
  name: "Hit the + button to add a new item."
});

const item3 = new Item({
  name: "<-- Hit this to delete an item."
});

// Store the default items in an array
const defaultItems = [item1, item2, item3];

// Define the Mongoose schema for to-do lists
const listSchema = new mongoose.Schema({
  name: String,
  items: [itemsSchema]
});

// Create a List Mongoose model based on the schema
const List = mongoose.model("List", listSchema);

// Handle GET requests to the root path
app.get("/", function (req, res) {

  // Find all items in the Item collection
  Item.find().then(founditems => {

    // If there are no items in the collection, insert the default items
    if (founditems.length === 0) {
      Item.insertMany(defaultItems).then(() => {
        console.log("Successfuly saved default items to DB.");
      }).catch(err => { console.log(err); })

      // Redirect to the root path to display the items
      res.redirect("/");
    } else {
      // Render the "list" template with the found items
      res.render("list", { listTitle: "Today", newListItems: founditems });
    }

  }).catch(err => { console.log(err); })

});

// Route for handling requests to a custom list page
app.get("/:customListName", function (req, res) {
  const customListName = _.capitalize(req.params.customListName);

  // Check if a list with the given name exists in the database
  List.findOne({ name: customListName })
    .then(foundList => {
      // Check if a list with the given name exists in the database
      if (!foundList) {
        // If the list doesn't exist, create a new one with default items and save it to the database.
        const list = new List({
          name: customListName,
          items: defaultItems
        });
        list.save()
          .then(() => {
            // Redirect the user to the new list's page
            res.redirect("/" + customListName);
          })
          .catch(err => {
            console.log("Error saving new list to the database:", err);
          });
      } else {

        // If the list exists, render its items to the list view
        res.render("list", { listTitle: foundList.name, newListItems: foundList.items });
      }
    })
    .catch(err => {
      console.log("Error finding list:", err);
    });
});

// Define a route for handling POST requests to the home page ("/").
// The request body contains a new item to be added to a list.
app.post("/", function (req, res) {

  // Extract the name of the new item from the request body.
  const itemName = req.body.newItem;

  // Extract the name of the list where the new item will be added from the request body.
  const listName = req.body.list;

  // Create a new Item object with the name of the new item.
  const item = new Item({
    name: itemName
  });

  if (listName === "Today") {
    item.save()
      .then(() => {
        res.redirect("/");
      })
      .catch(err => {
        console.log(err);
      })
  }
  else {
    List.findOne({ name: listName })
      .then(foundList => {
        foundList.items.push(item);
        foundList.save();
        res.redirect("/" + listName);

      }).catch(err => {
        console.log(err);
      })
  }

});

// Define a route for handling POST requests to the "/delete" URL.
// The request body contains the ID of the item to be deleted from a list.
app.post("/delete", function (req, res) {

  // Extract the ID of the item to be deleted from the request body.
  const checkedItemId = req.body.checkbox;

  // Extract the name of the list where the item to be deleted belongs from the request body.
  const listName = req.body.list;

  // If the list name is "Today", remove the item from the database and redirect to the home page.
  // Otherwise, find the corresponding list in the database and remove the item from it. 
  if (listName === "Today") {
    Item.findByIdAndRemove(checkedItemId)
      .then(() => {
        console.log("Successfully deleted checked item.");
        res.redirect("/");
      })
      .catch(err => {
        console.log(err);
      });
  }
  else {
    List.findOne({ name: listName })
      .then(foundList => {
        if (foundList) {
          foundList.items.pull({ _id: checkedItemId });
          return foundList.save();
        }
      })
      .then(() => {
        res.redirect("/" + listName);
      })
      .catch((err) => {
        console.log(err);
      });
  }
});

let port = process.env.PORT;
if (port == null || port==""){
  port = 3000;
}
// Start the server on port 3000 and log a message to the console
app.listen(port, function () {
  console.log("Server has started successfully.");
});

